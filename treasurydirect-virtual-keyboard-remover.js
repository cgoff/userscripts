// ==UserScript==
// @name        TreasuryDirect Virtual Keyboard Remover
// @namespace   Violentmonkey Scripts
// @match       https://*.treasurydirect.gov/*
// @grant       none
// @version     1.0
// @author      cg
// @description 1/11/2023, 3:45:38 AM
// ==/UserScript==
document.querySelector(".pwordinput").removeAttribute("readonly")