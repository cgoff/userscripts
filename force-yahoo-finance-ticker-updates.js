// ==UserScript==
// @name         Force Yahoo Finance Ticker Updates
// @namespace    Violentmonkey Scripts
// @version      1.0
// @description  Prevent Yahoo! Finance stock tickers from pausing updates
// @author       cg
// @match        https://finance.yahoo.com/*
// @grant        none
// @run-at       document-idle
// ==/UserScript==

(function () {
    // Ensure the script runs only on the desired page
    if (!window.finStreamer) {
        console.log("finStreamer not found. Retrying...");
        const observer = new MutationObserver(() => {
            if (window.finStreamer) {
                console.log("finStreamer found! Overriding pauseSubscription.");
                window.finStreamer.pauseSubscription = function () {
                    console.log("pauseSubscription prevented.");
                };
                observer.disconnect(); // Stop observing once done
            }
        });

        observer.observe(document.body, { childList: true, subtree: true });
        return;
    }

    // Override pauseSubscription
    window.finStreamer.pauseSubscription = function () {
        console.log("pauseSubscription prevented.");
    };
})();