// ==UserScript==
// @name        Profanity Filter
// @namespace   Violentmonkey Scripts
// @include     *
// @version     1
// @author      cg
// @description Simple Profanity Filter that utilizes a remote list.
// @grant       GM.getValue
// @grant       GM.setValue
// ==/UserScript==

// If desired, replace the URL below with another list. List should be plain text, and have one word per line.

(async function() {
  const storageKey = "profanityList";
  const storedList = await GM.getValue(storageKey, null);

  let profanityList;
  if (storedList !== null) {
    profanityList = JSON.parse(storedList);
  } else {
    const response = await fetch('https://raw.githubusercontent.com/coffee-and-fun/google-profanity-words/main/data/list.txt');
    const text = await response.text();
    profanityList = text.trim().split('\n');
    GM.setValue(storageKey, JSON.stringify(profanityList));
  }

  function replaceProfanity(match) {
    return match.substring(0, 2) + "*".repeat(match.length - 2);
  }

  function scanTextNode(node) {
    const regex = new RegExp("\\b(?:" + profanityList.join('|') + ")\\b", "gi");
    if (node.nodeType === Node.TEXT_NODE && regex.test(node.textContent)) {
      node.textContent = node.textContent.replace(regex, replaceProfanity);
    }
  }

  const nodeIterator = document.createNodeIterator(document.body, NodeFilter.SHOW_TEXT);
  let node;
  while (node = nodeIterator.nextNode()) {
    scanTextNode(node);
  }
})();
