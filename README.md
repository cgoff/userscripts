# userscripts

Public repository of useful [UserScripts](https://en.wikipedia.org/wiki/Userscript) I've written.

## Profanity Filter

This UserScript will download a remote list of words (in this case, profanity in the English language) and censor them when found on a webpage (e.g. "test" would be changed to "teXX").

## TreasuryDirect Virtual Keyboard Remover

Simple UserScript to enable use of your keyboard to enter a password (or copy and paste) rather than using the on-screen virtual keyboard. Use complex passwords without the burden of entering characters one-by-one, which encourages convenience over security.
